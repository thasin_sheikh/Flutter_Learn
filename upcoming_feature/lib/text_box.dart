import 'package:flutter/material.dart';

class TextBox extends StatelessWidget {
  final String _text1;
  final String _text2;

  const TextBox({super.key, required String text1, required String text2}):_text1=text1,_text2=text2;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          _text1,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontStyle: FontStyle.normal,
            fontFamily: 'Metropolis_Regular',
            fontWeight: FontWeight.w700,
            fontSize: 20,
            color: Color(0xFF0C1F23),
          ),
        ),
        const SizedBox(height: 4,),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 46,vertical: 4),
          child: Text(
            _text2,
            textAlign: TextAlign.center,
            maxLines: 3,
           // overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontStyle: FontStyle.normal,
              fontFamily: 'Metropolis_Regular',
              fontWeight: FontWeight.w400,
              fontSize: 14,
              color: Color(0xFF545454),
            ),
          ),
        ),
      ],
    );
  }
}
