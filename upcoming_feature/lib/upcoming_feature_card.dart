import 'package:flutter/material.dart';
import 'package:upcoming_feature/image_view.dart';
import 'package:upcoming_feature/text_box.dart';

class UpcomingFeatureCard extends StatelessWidget {

  const UpcomingFeatureCard ({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only( left: 20,right: 20),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
        child: Column(

          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const ImageView(imgpath:'assets/images/Grey icons medium.svg' ),
            const SizedBox(height: 24),
            const TextBox(text1: "Add your bank account", text2: "Transfer your cash to bank anytime from anywhere."),
            const SizedBox(height: 24),
            _buildCreateUpComingButton()
          ],
        ),
      ),
    );
  }
}

Widget _buildCreateUpComingButton() {
  return FloatingActionButton.extended(
    backgroundColor: const Color(0xFFE6E6E6),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
    label: const Row(
      children: [
        Text(
          "Upcoming Feature",
          style: TextStyle(
              fontStyle: FontStyle.normal,
              fontFamily: 'Metropolis_Regular',
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Color(0xFF8A8A8A)),
        )
      ],
    ),
    onPressed: () {},
  );
}
