import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class ImageView extends StatelessWidget {
  final String _imgpath;
  const ImageView({super.key,required String imgpath}):_imgpath=imgpath;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(_imgpath,
        height: 100,
        width: 100,
        fit: BoxFit.cover,)
      ],

    );
  }
}
