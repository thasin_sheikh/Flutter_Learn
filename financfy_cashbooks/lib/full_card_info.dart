import 'package:financfy_cashbooks/identifier_with_creation_date.dart';
import 'package:financfy_cashbooks/user_info.dart';
import 'package:financfy_cashbooks/user_role.dart';
import 'package:flutter/material.dart';

class FullCardInfo extends StatelessWidget {
  final String _userInfoContactType;
  final String _userInfoContactValue;
  final String _userInfoAddressType;
  final String _userInfoAddressValue;
  final String _imgPath;
  final String _cashbookHeading;
  final String _cashbookCreationDate;
  final String _userRole;
  final String _userType;

  const FullCardInfo(
      {super.key,
      required String userInfoContactType,
      required String userInfoContactValue,
      required String userInfoAddressType,
      required String userInfoAddressValue,
      required String imgPath,
      required String cashbookHeading,
      required String cashbookCreationDate,
      required String userRole,
      required String userType})
      : _userInfoContactType = userInfoContactType,
        _userInfoContactValue= userInfoContactValue,
        _userInfoAddressType = userInfoAddressType ,
        _userInfoAddressValue = userInfoAddressValue,
        _imgPath = imgPath,
        _cashbookHeading = cashbookHeading,
        _cashbookCreationDate = cashbookCreationDate,
        _userType = userType,
        _userRole = userRole;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            IdentifierWithCreationDate(
              cashbookHeading: _cashbookHeading,
              cashbookCreationDate: _cashbookCreationDate,
              imgPath: _imgPath,
            ),
            const SizedBox(
              height: 8,
            ),
            const Divider(
              height: 1,
              thickness: 1,
              color: Color(0xFFF4F4FA),
            ),
            //need to change
            const SizedBox(height: 7.5),
            UserRole(userRole: _userRole, userType: _userType),
            const SizedBox(height: 7.5),
            UserInfo(type: _userInfoContactType, value: _userInfoContactValue),
            const SizedBox(height: 7.5),
            UserInfo(type: _userInfoAddressType, value: _userInfoAddressValue),
          ],
        ),
      ),
    );
  }
}
