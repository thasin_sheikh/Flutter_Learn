import 'package:flutter/material.dart';

class CustomTextStyle {
  static TextStyle textStyleCashBookHeading = const TextStyle(
    fontFamily: 'Metropolis_Regular',
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: Color(0xFF0C1F23),
    fontStyle: FontStyle.normal,
  );

  static TextStyle textStyleCashbookCreationDate = const TextStyle(
    fontFamily: 'Metropolis_Regular',
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: Color(0xFF8A8A8A),
    fontStyle: FontStyle.normal,
  );

  static TextStyle textStyleValue = const TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 12,
    fontFamily: 'Metropolis_Regular',
    fontStyle: FontStyle.normal,
    color: Color(0xFF333333),
  );
  static TextStyle textStyleType= const TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 12,
    fontFamily: 'Metropolis_Regular',
    fontStyle: FontStyle.normal,
    color: Color(0xFF8A8A8A),

  );
  static TextStyle textStyleUserRole= const TextStyle(
  fontWeight: FontWeight.w500,
  fontSize: 12,
  fontFamily: 'Metropolis_Regular',
  fontStyle: FontStyle.normal,
  color: Color(0xFF8A8A8A),
  );
  static TextStyle textStyleUserType= const TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: 10,
    fontFamily: 'Metropolis_Regular',
    fontStyle: FontStyle.normal,
  );
}
