import 'package:financfy_cashbooks/my_text_style.dart';
import 'package:flutter/material.dart';

class NameAndDate extends StatelessWidget {
  final String _cashBookHeading;
  final String _cashBookCreationDate;

  const NameAndDate(
      {super.key, required String heading, required String creationDate})
      : _cashBookHeading = heading,
        _cashBookCreationDate = creationDate;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(_cashBookHeading,

            style: CustomTextStyle.textStyleCashBookHeading),
        const SizedBox(
          height: 2,
        ),
        Text(
          _cashBookCreationDate,
          style:CustomTextStyle.textStyleCashbookCreationDate
          ),
      ],
    );
  }
}
