import 'package:financfy_cashbooks/name_with_photo.dart';
import 'package:flutter/material.dart';

class IdentifierWithCreationDate extends StatelessWidget {
  final String _cashbookHeading;
  final String _cashbookCreationDate;
  final String _imgPath;

  const IdentifierWithCreationDate(
      {super.key,
      required String cashbookHeading,
      required String cashbookCreationDate,
      required String imgPath})
      : _cashbookHeading = cashbookHeading,
        _cashbookCreationDate = cashbookCreationDate,
        _imgPath = imgPath;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
            child: NameWithPhoto(
                cashbookHeading: _cashbookHeading,
                cashbookCreationDate: _cashbookCreationDate,imgPath: _imgPath,)),
        const Column(
          children: [Icon(Icons.more_vert)],
        )
      ],
    );
  }
}
