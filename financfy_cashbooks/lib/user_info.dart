import 'package:financfy_cashbooks/my_text_style.dart';
import 'package:flutter/cupertino.dart';

class UserInfo extends StatelessWidget {
  final String _type;
  final String _value;

  const UserInfo({super.key, required String type, required String value}):_type=type,_value=value;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(_type,
        style:CustomTextStyle.textStyleType),
        Flexible(
          child: Text(_value, maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.end,
            style:  CustomTextStyle.textStyleValue),
        )
      ],

    );
  }
}
