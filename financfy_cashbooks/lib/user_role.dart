import 'package:financfy_cashbooks/my_text_style.dart';
import 'package:flutter/material.dart';

class UserRole extends StatelessWidget {
  final String _userRole;
  final String  _userType;
  const UserRole({super.key,required String userRole, required String userType}):_userRole=userRole,_userType=userType;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
         Text(
          _userRole,
          style:CustomTextStyle.textStyleUserRole
        ),
        Flexible(
          child: Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
              color: Color(0xFFE6F0F2),
            ),
            child:  Padding(
              padding:
                  const EdgeInsets.only(top: 3.0, bottom: 3.0, left: 6.0, right: 6.0),
              child: Text(
                _userType,
                maxLines: 1,
                style: CustomTextStyle.textStyleUserType,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
