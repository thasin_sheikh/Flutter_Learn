import 'package:financfy_cashbooks/name_and_date.dart';
import 'package:flutter/material.dart';

class NameWithPhoto extends StatelessWidget {
  final String _cashbookHeading;
  final String _cashbookCreationDate;
  final String _imgPath;

  const NameWithPhoto(
      {super.key,
      required String cashbookHeading,
      required String cashbookCreationDate,
      required String imgPath})
      : _cashbookHeading = cashbookHeading,
        _cashbookCreationDate = cashbookCreationDate,
        _imgPath = imgPath;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(40),
              child:  Image(
                image: AssetImage(_imgPath),
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            )
          ],
        ),
        const SizedBox(width: 12),
        Expanded(
            child: NameAndDate(
                heading: _cashbookHeading,
                creationDate: _cashbookCreationDate)),
      ],
    );
  }
}
